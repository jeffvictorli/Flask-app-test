# New Flask app test

* https://ianlondon.github.io/blog/deploy-flask-docker-nginx/

## Dockerfile

```
FROM tiangolo/uwsgi-nginx-flask:python3.6

# copy over our requirements.txt file
COPY requirements.txt /tmp/

# upgrade pip and install required python packages
RUN pip install -U pip
RUN pip install -r /tmp/requirements.txt

# copy over our app code
COPY ./app /app

# set an environmental variable, MESSAGE,
# which the app will use and display
ENV MESSAGE "hello from Docker"

```

## Build

`docker build -t flask_ascii .`

## Run persistently

`docker run -d --restart=always -p 80:80 -t flask_ascii`
